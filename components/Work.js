import React from "react";
import Footer from "../components/Footer";

const Work = () => {
    return (
        <div>
            <div className="work" id="work">
                <h1 className="md:text-4xl lg:text-6xl">My Work</h1>
                <div className="lg:grid lg:grid-cols-4">
                    <div className="work-card">
                        <div>
                            <a target="_blank" rel="noopener" href="http://jeanpibot.gitlab.io/novillo/index.html">
                                <img
                                    className="w-full rounded-lg hover:zoom"
                                    src="../static/Novillo.webp"
                                    srcSet="../static/Novillo.webp, ../static/Novillo@2x.webp 2x"
                                    alt="Novillo"
                                />
                            </a>
                        </div>
                        <div className="relative -mt-2 mx-5 p-2 bg-white rounded-lg shadow-lg">
                            <h2 className="md:text-xl lg:text-2xl"> El Gran Novillo</h2>
                            <p className="leading-snug text-gray-700">
                                This Project introduces a website of company trade beef which is made from  Florencia Caquetá Colombia.
                            </p>
                        </div>
                    </div>
                    <div className="work-card">
                        <div>
                            <a
                            target="_blank"
                            rel="noopener"
                            href="https://jeanpibot.gitlab.io/Deca/"
                            >
                            <img
                                className="rounded-lg w-full hover:zoom"
                                src="../static/Deca.webp"
                                srcSet="../static/Deca.webp, ../static/Deca@2x.webp 2x"
                                alt="Deca"
                            />
                            </a>
                        </div>
                        <div className="relative -mt-2 mx-5 p-2 bg-white rounded-lg shadow-lg">
                            <h2 className="md:text-xl lg:text-2xl">Deca</h2>
                            <p className="leading-snug text-gray-700">
                            Project consists to build a website about doctorate in Education and Cultural environment of the University of Amazon from Florencia Caquetá.
                            </p>
                        </div>
                    </div>
                    <div className="work-card">
                        <div>
                            <a
                            target="_blank"
                            rel="noopener"
                            href="https://derivada-1xa823ldf.now.sh/Derivada.html"
                            >
                            <img
                                className="rounded-lg w-full hover:zoom"
                                src="../static/Derivada.webp"
                                srcSet="../static/Derivada.webp, ../static/Derivada@2x.webp 2x"
                                alt="Derivada"
                            />
                            </a>
                        </div>
                        <div className="relative -mt-2 mx-5 p-2 bg-white rounded-lg shadow-lg">
                            <h2 className="md:text-xl lg:text-2xl">Derivada</h2>
                            <p className="leading-snug text-gray-700">
                                This Project is looking to teach whole of subjects related Derivatives at engineers who course first year in the colege Universidad de la Amazonia.
                            </p>
                        </div>
                    </div>
                    <div className="work-card">
                        <div>
                            <a
                            target="_blank"
                            rel="noopener"
                            href="https://project-dpv32g81p.now.sh/"
                            >
                            <img
                                className="rounded-lg w-full hover:zoom"
                                src="../static/Moon.webp"
                                srcSet="../static/Moon.webp, ../static/Moon@2x.webp 2x"
                                alt="Moon"
                            />
                            </a>
                        </div>
                        <div className="relative -mt-2 mx-5 p-2 bg-white rounded-lg shadow-lg">
                            <h2 className="md:text-xl lg:text-2xl">Moon</h2>
                            <p className="leading-snug text-gray-700">
                                The project aims is to practice knowledge about websites built with Vue.js, Bulma and Uikit technology. Page can calculate your mass on the moon in real time.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <Footer></Footer>
        </div>
    );
};

export default Work;
