import React from "react";

const Services = () => {
    return (
        <div className="services" id="services">
            <h1 className="md:text-4xl lg:text-6xl">What I do</h1>
            <div className="md:grid md:grid-cols-3">
                <div className="mt-2">
                    <h3 className="md:text-xl lg:text-2xl"> Development </h3>
                    <p className="services-parrafo md:text-xl">
                        Any website is developed with current technologies like Html5, Css3, JavaScript, Go, Express, Node.js. At the same time, I know different frameworks and libraries like Next, React, Vue, Svelte, Bulma, Materialize and Tailwind
                    </p>
                </div>
                <div className="mt-2">
                    <h3 className="md:text-xl lg:text-2xl"> Design </h3>
                    <p className="services-parrafo md:text-xl">
                        Different types of designs are made with the keynote app which allows to create beautiful designs, besides it can make presentations, illustrations, videos and more.
                    </p>
                </div>
                <div className="mt-2">
                    <h3 className="md:text-xl lg:text-2xl"> Unity </h3>
                    <p className="services-parrafo md:text-xl">
                        2D and 3D augmented reality or virtual reality video games are created with Unity software.
                    </p>
                </div>
            </div>
            <div className="flex justify-center">
                <button className="boton transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 cursor-pointer">
                    <a  href="https://gitlab.com/JeanPiBot" target="_blank" rel="noopener">
                        My Gitlab
                    </a>
                </button>
            </div>
        </div>
    );
};

export default Services;
