import React from 'react';


const AboutMe = () => {
    return(
        <div className="mx-8 grid gap-6 py-10" id="about-me">
            <h1 className="md:text-4xl lg:text-6xl"> Who I am</h1>
            <p className="bg-indigo-200 font-bold lg:text-2xl tracking-tight text-center">Jean Pierre Giovanni Arenas Ortiz</p>
            <div className="text-justify text-base md:text-xl lg:text-2xl">
                <p>I am a Web Developer with two years of experiences in education. I am Looking to collaborate in any position about creating web sites due my reaches and goals got. Beside I have certificates of Platzi which is a platform Education online. It has Many courses as programming, design, marketing, web development, Frontend, Backend, Mobile dev, UX, etc.</p>
            </div>
            <figure className="mx-auto shadow-lg rounded-6">
                <img src="../static/profesional.webp" srcSet="../static/profesional.webp, ../static/profesional@2x.webp 2x" alt="Jean Pierre"/>
            </figure>
            <div className="flex justify-center">
                <button className="boton transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 cursor-pointer">
                    <a href="https://drive.google.com/open?id=1adEjx2OF8Du169u27E9BaBtW7OB7eETL" target="_blank" rel="noopener">
                        Download my CV
                    </a>
                </button>
            </div>
        </div>
    )
}

export default AboutMe