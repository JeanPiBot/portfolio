import React from 'react';

const Header = () => {
    return(
        <div className="intro">
            <h1 className="md:text-4xl lg:text-6xl">Hi, I am <strong className="capitalize">jean pierre giovanni arenas ortiz</strong></h1>
            <p className="bg-indigo-200 font-bold lg:text-2xl tracking-tight text-center">
                Frontend Developer
            </p>
            <figure className="mx-auto shadow-lg shadow-sm rounded transform skew-y-6">
                <img src="../static/jp.webp" srcSet="../static/jp.webp, ../static/jp@2x.webp 2x" alt="a picture of Jean Pierre" />
            </figure>
        </div>
    )
}

export default Header