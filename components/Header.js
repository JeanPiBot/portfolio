import React, { Component } from 'react';
class Nav extends Component {
    render () {
        return (
            <div className="header md:flex md:justify-around lg:flex-none lg:justify-between">
                <div className="logo">
                    <img src="../static/logo.webp" srcSet="../static/logo.webp, ../static/logo@2x.webp" alt="logo" />
                </div>
                <nav>
                    <ul className="nav-ul md:text-2xl md:grid md:grid-cols-4 md:gap-6">
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <a href="#services"> My services</a>
                        </li>
                        <li>
                            <a href="#about-me">About me</a>
                        </li>
                        <li>
                            <a href="#work">My work</a>
                        </li>
                    </ul>
                </nav>
            </div>
        )
    }
}


export default Nav